package com.random.number

import org.apache.commons.math3.random.RandomDataGenerator
import spock.lang.Specification

import static com.google.common.collect.Range.*

class DoubleGeneratorTest extends Specification {

    RandomDataGenerator randomDataGenerator = Mock()

    def "Should Generate Big Doubles Within Closed Range"() {
        given:
        final def start = 5.1d
        final def end = 15.8d
        final def expectedDouble = 135.8d
        randomDataGenerator.nextUniform(start.doubleValue(), end.doubleValue()) >> expectedDouble.toDouble()

        when:
        final def actualDouble = new DoubleGenerator(randomDataGenerator, closed(start, end), 4).next();

        then:
        actualDouble <=> expectedDouble == 0
    }

    def "Should Generate Big Doubles Within Open Range"() {
        given:
        final def start = 5.12d
        final def end = 15.81d
        final def expectedDouble = 23.7d
        randomDataGenerator.nextUniform(start.doubleValue() + 0.01d, end.doubleValue() - 0.01d) >> expectedDouble.toDouble()

        when:
        final def actualDouble = new DoubleGenerator(randomDataGenerator, open(start, end), 2).next();

        then:
        actualDouble <=> expectedDouble == 0
    }

    def "Should Generate Big Doubles Within Open Closed Range"() {
        given:
        final def start = 15.1d
        final def end = 135.8d
        final def expectedDouble = 135.8d
        randomDataGenerator.nextUniform(start.doubleValue() + 0.01d, end.doubleValue()) >> expectedDouble.toDouble()

        when:
        final def actualDouble = new DoubleGenerator(randomDataGenerator, openClosed(start, end), 2).next();

        then:
        actualDouble <=> expectedDouble == 0
    }

    def "Should Generate Big Doubles Within Closed Open Range"() {
        given:
        final def start = 54.13d
        final def end = 344.8d
        final def expectedDouble = 542.2d
        randomDataGenerator.nextUniform(start.doubleValue(), end.doubleValue() - 0.01d) >> expectedDouble.toDouble()

        when:
        final def actualDouble = new DoubleGenerator(randomDataGenerator, closedOpen(start, end), 2).next();

        then:
        actualDouble <=> expectedDouble == 0
    }

    def "Should Generate Big Doubles At Most"() {
        given:
        final def end = 15.8d
        final def expectedDouble = 15.4d
        randomDataGenerator.nextUniform(-Double.MAX_VALUE, end.doubleValue()) >> expectedDouble.toDouble()

        when:
        final def actualDouble = new DoubleGenerator(randomDataGenerator, atMost(end), 2).next();

        then:
        actualDouble <=> expectedDouble == 0
    }

    def "Should Generate Big Doubles Less Than"() {
        given:
        final def end = 75.25d
        final def expectedDouble = 542.2d
        randomDataGenerator.nextUniform(-Double.MAX_VALUE, end.doubleValue() - 0.01d) >> expectedDouble.toDouble()

        when:
        final def actualDouble = new DoubleGenerator(randomDataGenerator, lessThan(end), 2).next();

        then:
        actualDouble <=> expectedDouble == 0
    }

    def "Should Generate Big Doubles At Least"() {
        given:
        final def start = 15.8d
        final def expectedDouble = 542.2d
        randomDataGenerator.nextUniform(start, Double.MAX_VALUE) >> expectedDouble.toDouble()

        when:
        final def actualDouble = new DoubleGenerator(randomDataGenerator, atLeast(start), 2).next();

        then:
        actualDouble <=> expectedDouble == 0
    }

    def "Should Generate Big Doubles Greater Than"() {
        given:
        final def start = 12.2d
        final def expectedDouble = 15.4d
        randomDataGenerator.nextUniform(start + 0.01d, Double.MAX_VALUE) >> expectedDouble.toDouble()

        when:
        final def actualDouble = new DoubleGenerator(randomDataGenerator, greaterThan(start), 2).next();

        then:
        actualDouble <=> expectedDouble == 0
    }

    def "Should Generate Between minus and plus the max values of a double by default"() {
        given:
        final def expectedDouble = 542.2d
        randomDataGenerator.nextUniform(-Double.MAX_VALUE, Double.MAX_VALUE) >> expectedDouble.toDouble()

        when:
        final def actualDouble = new DoubleGenerator(randomDataGenerator, all(), 2).next();

        then:
        actualDouble <=> expectedDouble == 0
    }

    def "Should round generated numbers to the given scale"(def scale) {
        given:
        final def expectedDouble = 15.44567122d
        randomDataGenerator.nextUniform(_, _) >> expectedDouble.toDouble()

        when:
        final def actualDouble = new DoubleGenerator(randomDataGenerator, all(), scale).next();

        then:
        actualDouble.toBigDecimal().scale == scale

        where:
        scale << [3, 4, 5]
    }


}