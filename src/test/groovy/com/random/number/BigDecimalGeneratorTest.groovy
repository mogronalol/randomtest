package com.random.number

import org.apache.commons.math3.random.RandomDataGenerator
import spock.lang.Specification

import static com.google.common.collect.Range.*

class BigDecimalGeneratorTest extends Specification {

    RandomDataGenerator randomDataGenerator = Mock()

    def "Should Generate Big Decimals Within Closed Range"() {
        given:
        def start = 5.1
        def end = 15.8
        def expectedBigDecimal = 135.8
        randomDataGenerator.nextUniform(start.doubleValue(), end.doubleValue()) >> expectedBigDecimal.toDouble()

        when:
        def actualBigDecimal = new BigDecimalGenerator(randomDataGenerator, closed(start, end), 4).next();

        then:
        actualBigDecimal <=> expectedBigDecimal == 0
    }

    def "Should Generate Big Decimals Within Open Range"() {
        given:
        def start = 5.12
        def end = 15.81
        def expectedBigDecimal = 23.7
        randomDataGenerator.nextUniform(start.doubleValue() + 0.01, end.doubleValue() - 0.01) >> expectedBigDecimal.toDouble()

        when:
        def actualBigDecimal = new BigDecimalGenerator(randomDataGenerator, open(start, end), 2).next();

        then:
        actualBigDecimal <=> expectedBigDecimal == 0
    }

    def "Should Generate Big Decimals Within Open Closed Range"() {
        given:
        def start = 15.1
        def end = 135.8
        def expectedBigDecimal = 135.8
        randomDataGenerator.nextUniform(start.doubleValue() + 0.01, end.doubleValue()) >> expectedBigDecimal.toDouble()

        when:
        def actualBigDecimal = new BigDecimalGenerator(randomDataGenerator, openClosed(start, end), 2).next();

        then:
        actualBigDecimal <=> expectedBigDecimal == 0
    }

    def "Should Generate Big Decimals Within Closed Open Range"() {
        given:
        def start = 54.13
        def end = 344.8
        def expectedBigDecimal = 542.2
        randomDataGenerator.nextUniform(start.doubleValue(), end.doubleValue() - 0.01) >> expectedBigDecimal.toDouble()

        when:
        def actualBigDecimal = new BigDecimalGenerator(randomDataGenerator, closedOpen(start, end), 2).next();

        then:
        actualBigDecimal <=> expectedBigDecimal == 0
    }

    def "Should Generate Big Decimals At Most"() {
        given:
        def end = 15.8
        def expectedBigDecimal = 15.4
        randomDataGenerator.nextUniform(-Double.MAX_VALUE, end.doubleValue()) >> expectedBigDecimal.toDouble()

        when:
        def actualBigDecimal = new BigDecimalGenerator(randomDataGenerator, atMost(end), 2).next();

        then:
        actualBigDecimal <=> expectedBigDecimal == 0
    }

    def "Should Generate Big Decimals Less Than"() {
        given:
        def end = 75.25
        def expectedBigDecimal = 542.2
        randomDataGenerator.nextUniform(-Double.MAX_VALUE, end.doubleValue() - 0.01) >> expectedBigDecimal.toDouble()

        when:
        def actualBigDecimal = new BigDecimalGenerator(randomDataGenerator, lessThan(end), 2).next();

        then:
        actualBigDecimal <=> expectedBigDecimal == 0
    }

    def "Should Generate Big Decimals At Least"() {
        given:
        def start = 15.8
        def expectedBigDecimal = 542.2
        randomDataGenerator.nextUniform(start, Double.MAX_VALUE) >> expectedBigDecimal.toDouble()

        when:
        def actualBigDecimal = new BigDecimalGenerator(randomDataGenerator, atLeast(start), 2).next();

        then:
        actualBigDecimal <=> expectedBigDecimal == 0
    }

    def "Should Generate Big Decimals Greater Than"() {
        given:
        def start = 12.2
        def expectedBigDecimal = 15.4
        randomDataGenerator.nextUniform(start + 0.01, Double.MAX_VALUE) >> expectedBigDecimal.toDouble()

        when:
        def actualBigDecimal = new BigDecimalGenerator(randomDataGenerator, greaterThan(start), 2).next();

        then:
        actualBigDecimal <=> expectedBigDecimal == 0
    }

    def "Should Generate Between minus and plus the max values of a double by default"() {
        given:
        def expectedBigDecimal = 542.2
        randomDataGenerator.nextUniform(-Double.MAX_VALUE, Double.MAX_VALUE) >> expectedBigDecimal.toDouble()

        when:
        def actualBigDecimal = new BigDecimalGenerator(randomDataGenerator, all(), 2).next();

        then:
        actualBigDecimal <=> expectedBigDecimal == 0
    }

    def "Should round generated numbers to the given scale"(def scale) {
        given:
        def expectedBigDecimal = 15.44567122
        randomDataGenerator.nextUniform(_, _) >> expectedBigDecimal.toDouble()

        when:
        def actualBigDecimal = new BigDecimalGenerator(randomDataGenerator, all(), scale).next();

        then:
        actualBigDecimal.scale == scale

        where:
        scale << [3, 4, 5]
    }


}