package com.random.number

import org.apache.commons.math3.random.RandomDataGenerator
import spock.lang.Specification

import static com.google.common.collect.Range.*

class IntegerGeneratorTest extends Specification {

    RandomDataGenerator randomDataGenerator = Mock()

    def "Should Generate Integers Within Closed Range"() {
        given:
        def start = 5
        def end = 15
        def expectedInteger = 6
        randomDataGenerator.nextInt(start, end) >> expectedInteger.toInteger()

        when:
        def actualInteger = new IntegerGenerator(randomDataGenerator, closed(start, end)).next();

        then:
        actualInteger == expectedInteger
    }

    def "Should Generate Integers Within Open Range"() {
        given:
        def start = 111
        def end = 235
        def expectedInteger = 150
        randomDataGenerator.nextInt(start + 1, end - 1) >> expectedInteger.toInteger()

        when:
        def actualInteger = new IntegerGenerator(randomDataGenerator, open(start, end)).next();

        then:
        actualInteger == expectedInteger
    }

    def "Should Generate Integers Within Open Closed Range"() {
        given:
        def start = 400
        def end = 4000
        def expectedInteger = 405
        randomDataGenerator.nextInt(start + 1, end) >> expectedInteger.toInteger()

        when:
        def actualInteger = new IntegerGenerator(randomDataGenerator, openClosed(start, end)).next();

        then:
        actualInteger == expectedInteger
    }

    def "Should Generate Integers Within Closed Open Range"() {
        given:
        def start = 555
        def end = 888
        def expectedInteger = 558
        randomDataGenerator.nextInt(start, end - 1) >> expectedInteger.toInteger()

        when:
        def actualInteger = new IntegerGenerator(randomDataGenerator, closedOpen(start, end)).next();

        then:
        actualInteger == expectedInteger
    }

    def "Should Generate Integers At Most"() {
        given:
        def end = 667
        def expectedInteger = 554
        randomDataGenerator.nextInt(Integer.MIN_VALUE, end) >> expectedInteger.toInteger()

        when:
        def actualInteger = new IntegerGenerator(randomDataGenerator, atMost(end)).next();

        then:
        actualInteger == expectedInteger
    }

    def "Should Generate Integers Less Than"() {
        given:
        def end = 546546
        def expectedInteger = 4545
        randomDataGenerator.nextInt(Integer.MIN_VALUE, end - 1) >> expectedInteger.toInteger()

        when:
        def actualInteger = new IntegerGenerator(randomDataGenerator, lessThan(end)).next();

        then:
        actualInteger == expectedInteger
    }

    def "Should Generate Integers At Least"() {
        given:
        def start = 657
        def expectedInteger = 82846
        randomDataGenerator.nextInt(start, Integer.MAX_VALUE) >> expectedInteger.toInteger()

        when:
        def actualInteger = new IntegerGenerator(randomDataGenerator, atLeast(start)).next();

        then:
        actualInteger == expectedInteger
    }

    def "Should Generate Integers Greater Than"() {
        given:
        def start = 12
        def expectedInteger = 22
        randomDataGenerator.nextInt(start + 1, Integer.MAX_VALUE) >> expectedInteger.toInteger()

        when:
        def actualInteger = new IntegerGenerator(randomDataGenerator, greaterThan(start)).next();

        then:
        actualInteger == expectedInteger
    }

    def "Should Generate Between minus and plus the max values of a double by default"() {
        given:
        def expectedInteger = 542
        randomDataGenerator.nextInt(Integer.MIN_VALUE, Integer.MAX_VALUE) >> expectedInteger.toInteger()

        when:
        def actualInteger = new IntegerGenerator(randomDataGenerator, all()).next();

        then:
        actualInteger == expectedInteger
    }
}