package com.random.number

import org.apache.commons.math3.random.RandomDataGenerator
import spock.lang.Specification

import static com.google.common.collect.Range.*

class LongGeneratorTest extends Specification {

    RandomDataGenerator randomDataGenerator = Mock()

    def "Should Generate Longs Within Closed Range"() {
        given:
        def start = 5L
        def end = 15L
        def expectedLong = 6L
        randomDataGenerator.nextLong(start, end) >> expectedLong.toLong()

        when:
        def actualLong = new LongGenerator(randomDataGenerator, closed(start, end)).next();

        then:
        actualLong == expectedLong
    }

    def "Should Generate Longs Within Open Range"() {
        given:
        def start = 111L
        def end = 235L
        def expectedLong = 150L
        randomDataGenerator.nextLong(start + 1, end - 1) >> expectedLong.toLong()

        when:
        def actualLong = new LongGenerator(randomDataGenerator, open(start, end)).next();

        then:
        actualLong == expectedLong
    }

    def "Should Generate Longs Within Open Closed Range"() {
        given:
        def start = 400L
        def end = 4000L
        def expectedLong = 405L
        randomDataGenerator.nextLong(start + 1, end) >> expectedLong.toLong()

        when:
        def actualLong = new LongGenerator(randomDataGenerator, openClosed(start, end)).next();

        then:
        actualLong == expectedLong
    }

    def "Should Generate Longs Within Closed Open Range"() {
        given:
        def start = 555L
        def end = 888L
        def expectedLong = 558L
        randomDataGenerator.nextLong(start, end - 1) >> expectedLong.toLong()

        when:
        def actualLong = new LongGenerator(randomDataGenerator, closedOpen(start, end)).next();

        then:
        actualLong == expectedLong
    }

    def "Should Generate Longs At Most"() {
        given:
        def end = 667L
        def expectedLong = 554L
        randomDataGenerator.nextLong(Long.MIN_VALUE, end) >> expectedLong.toLong()

        when:
        def actualLong = new LongGenerator(randomDataGenerator, atMost(end)).next();

        then:
        actualLong == expectedLong
    }

    def "Should Generate Longs Less Than"() {
        given:
        def end = 546546L
        def expectedLong = 4545L
        randomDataGenerator.nextLong(Long.MIN_VALUE, end - 1) >> expectedLong.toLong()

        when:
        def actualLong = new LongGenerator(randomDataGenerator, lessThan(end)).next();

        then:
        actualLong == expectedLong
    }

    def "Should Generate Longs At Least"() {
        given:
        def start = 657L
        def expectedLong = 82846L
        randomDataGenerator.nextLong(start, Long.MAX_VALUE) >> expectedLong.toLong()

        when:
        def actualLong = new LongGenerator(randomDataGenerator, atLeast(start)).next();

        then:
        actualLong == expectedLong
    }

    def "Should Generate Longs Greater Than"() {
        given:
        def start = 12L
        def expectedLong = 22L
        randomDataGenerator.nextLong(start + 1, Long.MAX_VALUE) >> expectedLong.toLong()

        when:
        def actualLong = new LongGenerator(randomDataGenerator, greaterThan(start)).next();

        then:
        actualLong == expectedLong
    }

    def "Should Generate Between minus and plus the max values of a double by default"() {
        given:
        def expectedLong = 542L
        randomDataGenerator.nextLong(Long.MIN_VALUE, Long.MAX_VALUE) >> expectedLong.toLong()

        when:
        def actualLong = new LongGenerator(randomDataGenerator, all()).next();

        then:
        actualLong == expectedLong
    }
}