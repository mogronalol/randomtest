package com.random.datetime
import org.apache.commons.math3.random.RandomDataGenerator
import spock.lang.Specification

import java.time.LocalTime

import static com.google.common.collect.Range.*

class LocalTimeGeneratorTest extends Specification {

    RandomDataGenerator randomDataGenerator = Mock()

    def 'Should generate date within closed range'() {
        given:
        def start = LocalTime.of(1, 2, 4)
        def end = LocalTime.of(3, 4, 4)
        def expectedDateTime = LocalTime.of(4, 2, 4)
        def generator = new LocalTimeGenerator(randomDataGenerator, closed(start, end))
        randomDataGenerator.nextLong(start.toSecondOfDay(), end.toSecondOfDay()) >> expectedDateTime.toSecondOfDay()

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDateTime
    }

    def 'Should generate date within open range'() {
        given:
        def start = LocalTime.of(6, 3, 4)
        def end = LocalTime.of(7, 2, 4)
        def expectedDateTime = LocalTime.of(4, 2, 4)
        def generator = new LocalTimeGenerator(randomDataGenerator, open(start, end))
        randomDataGenerator.nextLong(start.toSecondOfDay() + 1, end.toSecondOfDay() - 1) >> expectedDateTime.toSecondOfDay()

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDateTime
    }

    def 'Should generate date within open closed range'() {
        given:
        def start = LocalTime.of(3, 2, 4)
        def end = LocalTime.of(7, 8, 4)
        def expectedDateTime = LocalTime.of(4, 2, 4)
        def generator = new LocalTimeGenerator(randomDataGenerator, openClosed(start, end))
        randomDataGenerator.nextLong(start.toSecondOfDay() + 1, end.toSecondOfDay()) >> expectedDateTime.toSecondOfDay()

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDateTime
    }

    def 'Should generate date within closed open range'() {
        given:
        def start = LocalTime.of(6, 2, 4)
        def end = LocalTime.of(8, 6, 4)
        def expectedDateTime = LocalTime.of(4, 2, 4)
        def generator = new LocalTimeGenerator(randomDataGenerator, closedOpen(start, end))
        randomDataGenerator.nextLong(start.toSecondOfDay(), end.toSecondOfDay() - 1) >> expectedDateTime.toSecondOfDay()

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDateTime
    }

    def 'Should generate date at most'() {
        given:
        def end = LocalTime.of(4, 1, 4)
        def expectedDateTime = LocalTime.of(7, 8, 4)
        def generator = new LocalTimeGenerator(randomDataGenerator, atMost(end))
        randomDataGenerator.nextLong(LocalTime.MIN.toSecondOfDay(), end.toSecondOfDay()) >> expectedDateTime.toSecondOfDay()

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDateTime
    }

    def 'Should generate date less than'() {
        given:
        def end = LocalTime.of(1, 2, 4)
        def expectedDateTime = LocalTime.of(6, 7, 4)
        def generator = new LocalTimeGenerator(randomDataGenerator, lessThan(end))
        randomDataGenerator.nextLong(LocalTime.MIN.toSecondOfDay(), end.toSecondOfDay() - 1) >> expectedDateTime.toSecondOfDay()

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDateTime
    }

    def 'Should generate date at least'() {
        given:
        def start = LocalTime.of(2, 1, 4)
        def expectedDateTime = LocalTime.of(8, 7, 4)
        def generator = new LocalTimeGenerator(randomDataGenerator, atLeast(start))
        randomDataGenerator.nextLong(start.toSecondOfDay(), LocalTime.MAX.toSecondOfDay()) >> expectedDateTime.toSecondOfDay()

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDateTime
    }

    def 'Should generate date greater than'() {
        given:
        def start = LocalTime.of(8, 2, 4)
        def expectedDateTime = LocalTime.of(2, 8, 4)
        def generator = new LocalTimeGenerator(randomDataGenerator, greaterThan(start))
        randomDataGenerator.nextLong(start.toSecondOfDay() + 1, LocalTime.MAX.toSecondOfDay()) >> expectedDateTime.toSecondOfDay()

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDateTime
    }
}
