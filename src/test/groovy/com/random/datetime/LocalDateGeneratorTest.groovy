package com.random.datetime

import org.apache.commons.math3.random.RandomDataGenerator
import spock.lang.Specification

import java.time.LocalDate

import static com.google.common.collect.Range.*

class LocalDateGeneratorTest extends Specification {

    RandomDataGenerator randomDataGenerator = Mock()

    def 'Should generate date within closed range'() {
        given:
        def start = LocalDate.of(2008, 1, 4);
        def end = LocalDate.of(2011, 5, 8);
        def expectedDate = LocalDate.of(1021, 5, 7)
        def generator = new LocalDateGenerator(randomDataGenerator, closed(start, end));
        randomDataGenerator.nextLong(start.toEpochDay(), end.toEpochDay()) >> expectedDate.toEpochDay()

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDate
    }


    def 'Should generate date within open range'() {
        given:
        def start = LocalDate.of(2008, 1, 4);
        def end = LocalDate.of(2014, 5, 8);
        def expectedDate = LocalDate.of(1541, 4, 9)
        def generator = new LocalDateGenerator(randomDataGenerator, open(start, end));
        randomDataGenerator.nextLong(start.toEpochDay() + 1, end.toEpochDay() - 1) >> expectedDate.toEpochDay()

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDate
    }


    def 'Should generate date within open closed range'() {
        given:
        def start = LocalDate.of(2017, 6, 4);
        def end = LocalDate.of(2018, 1, 8);
        def expectedDate = LocalDate.of(1067, 2, 5)
        def generator = new LocalDateGenerator(randomDataGenerator, openClosed(start, end));
        randomDataGenerator.nextLong(start.toEpochDay() + 1, end.toEpochDay()) >> expectedDate.toEpochDay()

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDate
    }


    def 'Should generate date within closed open range'() {
        given:
        def start = LocalDate.of(2011, 2, 4);
        def end = LocalDate.of(2014, 3, 8);
        def expectedDate = LocalDate.of(1078, 7, 1)
        def generator = new LocalDateGenerator(randomDataGenerator, closedOpen(start, end));
        randomDataGenerator.nextLong(start.toEpochDay(), end.toEpochDay() - 1) >> expectedDate.toEpochDay()

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDate

    }

    def 'Should generate date at most'() {
        given:
        def end = LocalDate.of(2012, 4, 8);
        def expectedDate = LocalDate.of(1022, 9, 1)
        def generator = new LocalDateGenerator(randomDataGenerator, atMost(end));
        randomDataGenerator.nextLong(LocalDate.MIN.toEpochDay(), end.toEpochDay()) >> expectedDate.toEpochDay()

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDate
    }

    def 'Should generate date less than'() {
        given:
        def end = LocalDate.of(2011, 5, 8);
        def expectedDate = LocalDate.of(1011, 12, 2)
        def generator = new LocalDateGenerator(randomDataGenerator, lessThan(end));
        randomDataGenerator.nextLong(LocalDate.MIN.toEpochDay(), end.toEpochDay() - 1) >> expectedDate.toEpochDay()

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDate
    }

    def 'Should generate date at least'() {
        given:
        def start = LocalDate.of(2055, 6, 4);
        def expectedDate = LocalDate.of(1021, 6, 3)
        def generator = new LocalDateGenerator(randomDataGenerator, atLeast(start));
        randomDataGenerator.nextLong(start.toEpochDay(), LocalDate.MAX.toEpochDay()) >> expectedDate.toEpochDay()

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDate
    }

    def 'Should generate date greater than'() {
        given:
        def start = LocalDate.of(2066, 7, 4);
        def expectedDate = LocalDate.of(4021, 7, 4)
        def generator = new LocalDateGenerator(randomDataGenerator, greaterThan(start));
        randomDataGenerator.nextLong(start.toEpochDay() + 1, LocalDate.MAX.toEpochDay()) >> expectedDate.toEpochDay()

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDate
    }
}
