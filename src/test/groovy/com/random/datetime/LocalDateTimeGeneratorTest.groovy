package com.random.datetime

import org.apache.commons.math3.random.RandomDataGenerator
import spock.lang.Specification

import java.time.LocalDateTime
import java.time.ZoneOffset

import static com.google.common.collect.Range.*

class LocalDateTimeGeneratorTest extends Specification {

    RandomDataGenerator randomDataGenerator = Mock()

    def 'Should generate date within closed range'() {
        given:
        def start = LocalDateTime.of(2008, 1, 4, 1, 2, 4)
        def end = LocalDateTime.of(2011, 5, 8, 3, 4, 4)
        def expectedDateTime = LocalDateTime.of(1021, 5, 7, 4, 2, 4)
        def generator = new LocalDateTimeGenerator(randomDataGenerator, closed(start, end))
        randomDataGenerator.nextLong(start.toEpochSecond(ZoneOffset.UTC), end.toEpochSecond(ZoneOffset.UTC)) >> expectedDateTime.toEpochSecond(ZoneOffset.UTC)

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDateTime
    }

    def 'Should generate date within open range'() {
        given:
        def start = LocalDateTime.of(2008, 1, 4, 6, 3, 4)
        def end = LocalDateTime.of(2014, 5, 8, 7, 2, 4)
        def expectedDateTime = LocalDateTime.of(1541, 4, 9, 4, 2, 4)
        def generator = new LocalDateTimeGenerator(randomDataGenerator, open(start, end))
        randomDataGenerator.nextLong(start.toEpochSecond(ZoneOffset.UTC) + 1, end.toEpochSecond(ZoneOffset.UTC) - 1) >> expectedDateTime.toEpochSecond(ZoneOffset.UTC)

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDateTime
    }

    def 'Should generate date within open closed range'() {
        given:
        def start = LocalDateTime.of(2017, 6, 4, 3, 2, 4)
        def end = LocalDateTime.of(2018, 1, 8, 7, 8, 4)
        def expectedDateTime = LocalDateTime.of(1067, 2, 5, 4, 2, 4)
        def generator = new LocalDateTimeGenerator(randomDataGenerator, openClosed(start, end))
        randomDataGenerator.nextLong(start.toEpochSecond(ZoneOffset.UTC) + 1, end.toEpochSecond(ZoneOffset.UTC)) >> expectedDateTime.toEpochSecond(ZoneOffset.UTC)

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDateTime
    }

    def 'Should generate date within closed open range'() {
        given:
        def start = LocalDateTime.of(2011, 2, 4, 6, 2, 4)
        def end = LocalDateTime.of(2014, 3, 8, 8, 6, 4)
        def expectedDateTime = LocalDateTime.of(1078, 7, 1, 4, 2, 4)
        def generator = new LocalDateTimeGenerator(randomDataGenerator, closedOpen(start, end))
        randomDataGenerator.nextLong(start.toEpochSecond(ZoneOffset.UTC), end.toEpochSecond(ZoneOffset.UTC) - 1) >> expectedDateTime.toEpochSecond(ZoneOffset.UTC)

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDateTime
    }

    def 'Should generate date at most'() {
        given:
        def end = LocalDateTime.of(2012, 4, 8, 4, 1, 4)
        def expectedDateTime = LocalDateTime.of(1022, 9, 1, 7, 8, 4)
        def generator = new LocalDateTimeGenerator(randomDataGenerator, atMost(end))
        randomDataGenerator.nextLong(LocalDateTime.MIN.toEpochSecond(ZoneOffset.UTC), end.toEpochSecond(ZoneOffset.UTC)) >> expectedDateTime.toEpochSecond(ZoneOffset.UTC)

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDateTime
    }

    def 'Should generate date less than'() {
        given:
        def end = LocalDateTime.of(2011, 5, 8, 1, 2, 4)
        def expectedDateTime = LocalDateTime.of(1011, 12, 2, 6, 7, 4)
        def generator = new LocalDateTimeGenerator(randomDataGenerator, lessThan(end))
        randomDataGenerator.nextLong(LocalDateTime.MIN.toEpochSecond(ZoneOffset.UTC), end.toEpochSecond(ZoneOffset.UTC) - 1) >> expectedDateTime.toEpochSecond(ZoneOffset.UTC)

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDateTime
    }

    def 'Should generate date at least'() {
        given:
        def start = LocalDateTime.of(2055, 6, 4, 2, 1, 4)
        def expectedDateTime = LocalDateTime.of(1021, 6, 3, 8, 7, 4)
        def generator = new LocalDateTimeGenerator(randomDataGenerator, atLeast(start))
        randomDataGenerator.nextLong(start.toEpochSecond(ZoneOffset.UTC), LocalDateTime.MAX.toEpochSecond(ZoneOffset.UTC)) >> expectedDateTime.toEpochSecond(ZoneOffset.UTC)

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDateTime
    }

    def 'Should generate date greater than'() {
        given:
        def start = LocalDateTime.of(2066, 7, 4, 8, 2, 4)
        def expectedDateTime = LocalDateTime.of(4021, 7, 4, 2, 8, 4)
        def generator = new LocalDateTimeGenerator(randomDataGenerator, greaterThan(start))
        randomDataGenerator.nextLong(start.toEpochSecond(ZoneOffset.UTC) + 1, LocalDateTime.MAX.toEpochSecond(ZoneOffset.UTC)) >> expectedDateTime.toEpochSecond(ZoneOffset.UTC)

        when:
        def localDate = generator.next()

        then:
        localDate == expectedDateTime
    }
}
