package com.random.collections
import com.random.Generator
import spock.lang.Specification

class ListGeneratorTest extends Specification {

    private Generator<Object> elementGenerator = Mock()

    private Generator<Integer> sizeGenerator = Mock()

    def 'Should generate a list with five elements'() {
        given:
        sizeGenerator.next() >> 5

        def objects = [new Object(), new Object(), new Object(), new Object(), new Object()]
        elementGenerator.stream() >> objects.stream()

        when:
        def next = new ListGenerator<>(elementGenerator, sizeGenerator).next()

        then:
        next == objects
    }

    def 'Should generate a list with three elements'() {
        given:
        sizeGenerator.next() >> 3

        def objects = [new Object(), new Object(), new Object(), new Object(), new Object()]
        elementGenerator.stream() >> objects.stream()

        when:
        def next = new ListGenerator<>(elementGenerator, sizeGenerator).next()

        then:
        next == [objects[0], objects[1], objects[2]]
    }
}
