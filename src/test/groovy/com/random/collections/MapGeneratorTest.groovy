package com.random.collections
import com.random.Generator
import spock.lang.Specification

class MapGeneratorTest extends Specification {
    Generator<Integer> sizeGenerator = Mock()

    Generator<Object> keyGenerator = Mock()

    Generator<Object> valueGenerator = Mock()

    def 'Should generate a map with three keys and values'() {
        given:
        keyGenerator.next() >>> [1, 2, 3]
        valueGenerator.next() >>> [4, 5, 6]
        sizeGenerator.next() >> 3

        when:
        def map = new MapGenerator<>(keyGenerator, valueGenerator, sizeGenerator).next();

        then:
        map == [1:4, 2:5, 3:6]
    }

    def 'Should generate a map with five keys and values'() {
        given:
        keyGenerator.next() >>> [1, 2, 3, 4, 5]
        valueGenerator.next() >>> [4, 5, 6, 7, 8]
        sizeGenerator.next() >> 5

        when:
        def map = new MapGenerator<>(keyGenerator, valueGenerator, sizeGenerator).next();

        then:
        map == [1:4, 2:5, 3:6, 4:7, 5:8]
    }

    def 'Should generate a map with of the correct size even if the same keys are generated more than once'() {
        given:
        keyGenerator.next() >>> [1, 1, 2]
        valueGenerator.next() >>> [4]
        sizeGenerator.next() >> 2

        when:
        def map = new MapGenerator<>(keyGenerator, valueGenerator, sizeGenerator).next();

        then:
        map == [1:4, 2:4]
    }
}
