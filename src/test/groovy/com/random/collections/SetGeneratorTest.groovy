package com.random.collections
import com.random.Generator
import spock.lang.Specification

class SetGeneratorTest extends Specification{

    Generator<Object> elementGenerator = Mock()

    Generator<Integer> sizeGenerator = Mock()

    def 'Should generate a set with five elements'() {
        given:
        def objects = [new Object(), new Object(), new Object(), new Object(), new Object()]
        sizeGenerator.next() >> 5
        elementGenerator.stream() >> objects.stream()

        when:
        def set = new SetGenerator<>(elementGenerator, sizeGenerator).next();

        then:
        set == objects as Set
    }

    def 'Should generate a set with three elements'() {
        given:
        def objects = [new Object(), new Object(), new Object(), new Object(), new Object()]
        sizeGenerator.next() >> 3
        elementGenerator.stream() >> objects.stream()

        when:
        def set = new SetGenerator<>(elementGenerator, sizeGenerator).next();

        then:
        set == [objects[0], objects[1], objects[2]] as Set
    }

    def 'Should generate a set which is the correct size even if the stream contains duplicates'() {
        given:
        def duplicate = new Object()
        def objects = [new Object(), duplicate, duplicate, duplicate, new Object()]
        sizeGenerator.next() >> 3
        elementGenerator.stream() >> objects.stream()

        when:
        def set = new SetGenerator<>(elementGenerator, sizeGenerator).next();

        then:
        set == [objects[0], duplicate, objects[4]] as Set
    }
}
