package com.random.utils

import static com.google.common.collect.Range.closed

class DiscreteUniformDistribution {
    private final BigDecimal startInclusive;
    private final BigDecimal endInclusive;
    private final int repeats;

    private DiscreteUniformDistribution(final def startInclusive, final def endInclusive, final int repeats = 0) {
        this.startInclusive = startInclusive.toBigDecimal()
        this.endInclusive = endInclusive.toBigDecimal()
        this.repeats = repeats
    }

    static DiscreteUniformDistribution betweenDecimalsAsIntegers(final BigDecimal startInclusive, final BigDecimal endInclusive, final int scale, final int repeats = 0) {
        return new DiscreteUniformDistribution(startInclusive.movePointRight(scale).toInteger(),
                endInclusive.movePointRight(scale).toInteger(), repeats)
    }

    static DiscreteUniformDistribution betweenIntegers(final int startInclusive, final int endInclusive, final int repeats = 0) {
        return new DiscreteUniformDistribution(startInclusive, endInclusive, repeats)
    }

    static DiscreteUniformDistribution betweenLongs(final long startInclusive, final long endInclusive, final int repeats = 0) {
        return new DiscreteUniformDistribution(startInclusive, endInclusive, repeats)
    }

    BigDecimal getVariance() {
        (((endInclusive - startInclusive + 1).power(2) - 1) / 12)
    }

    BigDecimal getStandardDeviation() {
        Math.sqrt(variance)
    }

    BigDecimal getStandardDeviationOfTheMean() {
        1 / Math.sqrt(amountOfValues) * standardDeviation
    }

    long getAmountOfValues() {
        (endInclusive - startInclusive + 1) * (repeats + 1)
    }

    BigDecimal getMean() {
        ((endInclusive + startInclusive) / 2)
    }

    com.google.common.collect.Range<BigDecimal> getTwoStandardDeviationsOfTheMeanFromTheMean() {
        def mean = mean
        def twoStandardDeviationsOfTheMean = standardDeviationOfTheMean * 2
        return closed(mean - twoStandardDeviationsOfTheMean, mean + twoStandardDeviationsOfTheMean)
    }
}
