package com.random.utils
import com.random.Generator

class MeanTester {
    private static final int ITERATIONS = 10_000

    private final DiscreteUniformDistribution distribution
    private final Generator<Integer> generator
    private final Closure generatedElementToDouble

    MeanTester(DiscreteUniformDistribution distribution, final Generator<?> generator, Closure generatedElementToDouble = {Double.valueOf(it)}) {
        this.distribution = distribution
        this.generator = generator
        this.generatedElementToDouble = generatedElementToDouble
    }

    BigDecimal percentageOfTimesMeanIsWithinTwoStandardDeviationsOfUniformMean() {

        def passcount = 0
        def range = distribution.twoStandardDeviationsOfTheMeanFromTheMean

        for (int i in 0..ITERATIONS - 1) {
            final BigDecimal mean = generator.stream()
                    .limit(distribution.amountOfValues)
                    .mapToDouble(generatedElementToDouble)
                    .average()
                    .asDouble

            passcount += range.contains(mean) ? 1 : 0
        }

        passcount / ITERATIONS * 100
    }


}