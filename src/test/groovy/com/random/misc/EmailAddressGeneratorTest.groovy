package com.random.misc

import com.random.Generator
import spock.lang.Specification


class EmailAddressGeneratorTest extends Specification {

    Generator leftGenerator = Mock()
    Generator rightPrefixGenerator = Mock()
    Generator rightSuffixGenerator = Mock()


    def 'Should generate left side, followed by @, followed by right prefix, followed by ".", followed by rightSuffix'(left,
                                                                                                                       rightPrefix,
                                                                                                                       rightSuffix) {
        given:
        def generator = new EmailAddressGenerator(leftGenerator, rightPrefixGenerator, rightSuffixGenerator)
        leftGenerator.next() >> left
        rightPrefixGenerator.next() >> rightPrefix
        rightSuffixGenerator.next() >> rightSuffix

        when:
        def emailAddress = generator.next()

        then:
        emailAddress == left + "@" + rightPrefix + "." + rightSuffix

        where:
        left        << ["adasd"  , "dfsdf"  , "fdfdsf"]
        rightPrefix << ["sdasd"  , "jdkdd"  , "sadsd"]
        rightSuffix << ["sdassd" , "jdkddsd", "sadsjd"]
    }
}
