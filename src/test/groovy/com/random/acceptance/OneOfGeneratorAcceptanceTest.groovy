package com.random.acceptance
import com.random.Generator
import com.random.utils.DiscreteUniformDistribution
import com.random.utils.MeanTester
import spock.lang.Specification

import java.util.stream.Collectors

class OneOfGeneratorAcceptanceTest extends Specification {
    def 'Should generate a mean within two standard deviations of the uniform mean'() {
        given:
        def values = 1..10
        def generator = Generator.ofOneOf(values)
        def distribution = DiscreteUniformDistribution.betweenIntegers(1, 10, 10)
        def meanTester = new MeanTester(distribution, generator)

        when:
        def percentage = meanTester.percentageOfTimesMeanIsWithinTwoStandardDeviationsOfUniformMean();

        then:
        percentage >= 0.95 && percentage <= 100
    }

    def 'Should generate every element'() {
        given:
        def values = 1..10
        def generator = Generator.ofOneOf(values)

        when:
        def elements = generator.stream().limit(100_000).collect(Collectors.toSet())

        then:
        elements.size() == 10
    }
}
