package com.random.acceptance

import com.random.Generator
import com.random.utils.DiscreteUniformDistribution
import com.random.utils.MeanTester
import spock.lang.Specification

import java.util.stream.Collectors

import static com.google.common.collect.Range.*
import static java.util.stream.Collectors.toSet

class DoubleGeneratorAcceptanceTest extends Specification {

    def 'Should generate random doubles'() {
        given:
        def generator = Generator.ofDoubles()

        when:
        def doubles = generator.stream().limit(1000).collect(toSet());

        then:
        doubles.size() == 1000
    }

    def 'Should generate a mean within two standard deviations of the uniform mean'() {
        given:
        def start = 0.18d
        def end = 0.21d
        def scale = Generator.DEFAULT_DECIMAL_SCALE
        def generator = Generator.ofDoubles(closed(start, end));
        def distribution = DiscreteUniformDistribution.betweenDecimalsAsIntegers(start, end, scale)
        def meanTester = new MeanTester(distribution, generator, {it.toBigDecimal().movePointRight(scale).toDouble()});

        when:
        def percentage = meanTester.percentageOfTimesMeanIsWithinTwoStandardDeviationsOfUniformMean();

        then:
        percentage >= 0.95 && percentage <= 100
    }

    def 'Should generate every number within range using correct scale'(def generator,
                                                                        def expectedMin,
                                                                        def expectedMax,
                                                                        def expectedSize,
                                                                        def expectedScale) {
        when:
        def results = generator.stream().limit(10_000_000).collect(Collectors.toSet());

        then:
        results.min() == expectedMin
        results.max() == expectedMax
        results.size() == expectedSize
        results.each {
            assert it.toBigDecimal().scale <= expectedScale
        }

        where:
        generator    << [Generator.ofDoubles(closed(0.25d, 0.56d)), Generator.ofDoubles(closedOpen(0.0058d, 0.012d), 5)]
        expectedMin  << [0.25d                          , 0.0058d]
        expectedMax  << [0.56d                          , 0.01199d]
        expectedSize << [3101                           , 620d]
        expectedScale <<[Generator.DEFAULT_DECIMAL_SCALE, 5]
    }

    def 'Should generate doubles within their specified ranges'(com.google.common.collect.Range<Double> range,
                                                        double expectedMin, double expectedMax) {
        given:
        Generator<Double> generator = Generator.ofDoubles(range);

        when:
        def results = generator.stream().limit(10_000).collect(Collectors.toList());

        then:
        results.min() == expectedMin
        results.max() == expectedMax

        where:
        range       << [closed(0.01d, 0.05d), closedOpen(0.02d, 0.06d), openClosed(0.03d, 0.04d), open(0.041d, 0.0481d)]
        expectedMin << [0.01d               , 0.02d                   , 0.0301d                 , 0.0411d]
        expectedMax << [0.05d               , 0.0599d                 , 0.04d                   , 0.048d]
    }

    def 'Should generate doubles with the specified scale'() {
        when:
        def results = generator.stream().limit(1000).collect(Collectors.toSet());

        then:
        results.each {
            assert it.toBigDecimal().scale <= Generator.DEFAULT_DECIMAL_SCALE
        }

        where:
        generator      << [Generator.ofDoubles(1), Generator.ofDoubles(2), Generator.ofDoubles(5)]
        expectedScale  << [1           , 2           , 5]
    }
}
