package com.random.acceptance
import com.random.Generator
import com.random.utils.DiscreteUniformDistribution
import com.random.utils.MeanTester
import spock.lang.Specification

import static com.google.common.collect.Range.closed
import static com.random.Generator.ofDoubles

public class StringGeneratorAcceptanceTest extends Specification {
    
    def 'Should generate a mean within two standard deviations of the uniform mean'() {
        given:
        def start = 0.18d
        def end = 0.21d
        def scale = Generator.DEFAULT_DECIMAL_SCALE
        def generator = ofDoubles(closed(start, end));
        def distribution = DiscreteUniformDistribution.betweenDecimalsAsIntegers(start, end, scale)
        def meanTester = new MeanTester(distribution, generator, {it.toBigDecimal().movePointRight(scale).toDouble()});

        when:
        def percentage = meanTester.percentageOfTimesMeanIsWithinTwoStandardDeviationsOfUniformMean();

        then:
        percentage >= 0.95 && percentage <= 100
    }

    def 'Should generate an alphanumeric string with a size between 5 and 15 inclusive by default'() {
        given:
        def generator = Generator.ofStrings()

        when:
        def strings = generator.stream().limit(100_000).collect() as Set
        def stringLengths = strings.collect { it.length() } as Set

        then:
        strings.forEach{
            assert it ==~ /^[A-Za-z0-9]*$/
        }
        stringLengths.min() == 5
        stringLengths.max() == 15
        stringLengths.size() == 11
    }

    def 'Should generate an alphanumeric string with a given size'(def length) {
        given:
        def generator = Generator.ofStrings({length})

        when:
        def strings = generator.stream().limit(100_000).collect() as Set
        def stringLengths = strings.collect { it.length() } as Set

        then:
        strings.forEach{
            assert it ==~ /^[A-Za-z0-9]*$/
        }
        stringLengths.size() == 1
        stringLengths.getAt(0) == length

        where:
        length << [4, 5, 8]
    }
}

