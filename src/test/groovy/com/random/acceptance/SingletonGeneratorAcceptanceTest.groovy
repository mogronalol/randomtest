package com.random.acceptance
import com.random.Generator
import spock.lang.Specification

import java.util.stream.Collectors

class SingletonGeneratorAcceptanceTest extends Specification{

    def "Should generate the same instance every time"() {
        given:
        def singleton = new Object();

        when:
        def singletons = Generator.ofSingleton(singleton).stream().limit(1000).collect(Collectors.toSet())

        then:
        singletons.size() == 1
    }
}
