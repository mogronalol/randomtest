package com.random.acceptance
import com.random.Generator
import com.random.utils.DiscreteUniformDistribution
import com.random.utils.MeanTester
import spock.lang.Specification

import java.time.LocalDate
import java.time.Period
import java.time.temporal.ChronoUnit

import static com.google.common.collect.Range.closed
import static java.time.LocalDate.now
import static java.util.stream.Collectors.toSet

class LocalDateGeneratorAcceptanceTest extends Specification {

    def 'Should generate random local dates'() {
        given:
        def generator = Generator.ofLocalDates()

        when:
        def localDates = generator.stream().limit(1000).collect(toSet());

        then:
        localDates.size() == 1000
    }

    def 'Should eventually generate every date in range'() {
        given:
        def start = LocalDate.of(2011, 8, 2)
        def end = LocalDate.of(2011, 10, 4)
        def generator = Generator.ofLocalDates(com.google.common.collect.Range.closed(start, end))

        when:
        def localDates = generator.stream().limit(1000).collect(toSet());

        then:
        localDates.size() == ChronoUnit.DAYS.between(start, end) + 1
        localDates.min() == start
        localDates.max() == end
    }

    def 'Should generate a mean within two standard deviations of the uniform mean'() {
        given:
        def start = LocalDate.of(2011, 8, 2)
        def end = LocalDate.of(2012, 10, 4)
        def generator = Generator.ofLocalDates(closed(start, end));
        def distribution = DiscreteUniformDistribution.betweenLongs(start.toEpochDay(), end.toEpochDay())
        def meanTester = new MeanTester(distribution, generator, {it.toEpochDay().toDouble()});

        when:
        def percentage = meanTester.percentageOfTimesMeanIsWithinTwoStandardDeviationsOfUniformMean();

        then:
        percentage >= 0.95 && percentage <= 100
    }

    def 'Should generate a local date in the future'() {
        given:
        def generator = Generator.ofFutureLocalDates();

        when:
        def localDates = generator.stream().limit(100_000).collect(toSet());

        then:
        localDates.forEach{
            assert it > now()
        }
    }

    def 'Should generate a local date in the past'() {
        given:
        def generator = Generator.ofPastLocalDates();

        when:
        def localDates = generator.stream().limit(100_000).collect(toSet());

        then:
        localDates.forEach{
            assert it < now()
        }
    }

    def 'Should generate future dates between tommorrow up until'(period, max) {
        given:
        def generator = Generator.ofFutureLocalDatesUpTo(period)

        when:
        def localDates = generator.stream().limit(1_000_000).collect(toSet())

        then:
        localDates.min() == now().plusDays(1)
        localDates.max() == max

        where:
        period << [Period.ofDays(10), Period.ofMonths(3), Period.ofWeeks(4)]
        max << [now().plusDays(10), now().plusMonths(3), now().plusWeeks(4)]
    }

    def 'Should generate past dates from yesterday up until'(period, min) {
        given:
        def generator = Generator.ofPastLocalDatesUpTo(period)

        when:
        def localDates = generator.stream().limit(1_000_000).collect(toSet())

        then:
        localDates.max() == now().minusDays(1)
        localDates.min() == min

        where:
        period << [Period.ofDays(10), Period.ofMonths(3), Period.ofWeeks(4)]
        min << [now().minusDays(10), now().minusMonths(3), now().minusWeeks(4)]
    }
}
