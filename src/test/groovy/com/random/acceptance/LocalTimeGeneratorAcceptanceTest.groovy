package com.random.acceptance
import com.random.Generator
import com.random.utils.DiscreteUniformDistribution
import com.random.utils.MeanTester
import spock.lang.Specification

import java.time.LocalTime
import java.time.temporal.ChronoUnit

import static com.google.common.collect.Range.closed
import static java.util.stream.Collectors.toSet

class LocalTimeGeneratorAcceptanceTest extends Specification {

    def 'Should generate random local times'() {
        given:
        def generator = Generator.ofLocalTimes()

        when:
        def localTimes = generator.stream().limit(50).collect(toSet());

        then:
        localTimes.size() == 50
    }

    def 'Should eventually generate every date in range'() {
        given:
        def start = LocalTime.of(4, 1, 8)
        def end = LocalTime.of(4, 1, 12)
        def generator = Generator.ofLocalTimes(closed(start, end))

        when:
        def localTimes = generator.stream().limit(100_000).collect(toSet());

        then:
        localTimes.size() == ChronoUnit.SECONDS.between(start, end) + 1
        localTimes.min() == start
        localTimes.max() == end
    }

    def 'Should generate a mean within two standard deviations of the uniform mean'() {
        given:
        def start = LocalTime.of(4, 1, 8)
        def end = LocalTime.of(4, 1, 12)
        def generator = Generator.ofLocalTimes(closed(start, end));
        def distribution = DiscreteUniformDistribution.betweenLongs(start.toSecondOfDay(), end.toSecondOfDay())
        def meanTester = new MeanTester(distribution, generator, {it.toSecondOfDay().toDouble()});

        when:
        def percentage = meanTester.percentageOfTimesMeanIsWithinTwoStandardDeviationsOfUniformMean();

        then:
        percentage >= 0.95 && percentage <= 100
    }
}
