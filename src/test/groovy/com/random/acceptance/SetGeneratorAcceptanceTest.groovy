package com.random.acceptance

import com.random.Generator
import spock.lang.Specification

import static java.util.stream.Collectors.toList

class SetGeneratorAcceptanceTest extends Specification {

    def 'Should generate a set only containing elements from the element generator between 5 and 15 in size'() {
        given:
        def generator = Generator.ofSets(Generator.ofOneOf(1..16))

        when:
        def generatedSets = generator.stream().limit(100_000).collect(toList())
        def sizesOfGeneratedSets = generatedSets.collect { it.size() }.toSet()

        then:
        sizesOfGeneratedSets.min() == 5
        sizesOfGeneratedSets.max() == 15
        generatedSets.flatten().toSet() == (1..16) as Set
    }

    def 'Should generate a set only containing elements from the element generator with the given size'(size) {
        given:
        def generator = Generator.ofSets(Generator.ofOneOf(1..16), {size})

        when:
        def generatedSets = generator.stream().limit(100_000).collect(toList())
        def sizesOfGeneratedSets = generatedSets.collect { it.size() }.toSet()

        then:
        sizesOfGeneratedSets.min() == size
        sizesOfGeneratedSets.max() == size
        generatedSets.flatten().toSet() == (1..16) as Set

        where:
        size << [2, 4, 7]
    }
}
