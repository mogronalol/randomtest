package com.random.acceptance

import com.random.Generator
import com.random.utils.DiscreteUniformDistribution
import com.random.utils.MeanTester
import spock.lang.Specification

class BooleanGeneratorAcceptanceTest extends Specification {

    def 'Should generate a mean within two standard deviations of the uniform mean'() {
        given:
        def generator = Generator.ofBooleans()
        def distribution = DiscreteUniformDistribution.betweenIntegers(1, 2, 500)
        def meanTester = new MeanTester(distribution, generator, {it ? 1d : 2d})

        when:
        def percentage = meanTester.percentageOfTimesMeanIsWithinTwoStandardDeviationsOfUniformMean()

        then:
        percentage > 0.95
    }
}
