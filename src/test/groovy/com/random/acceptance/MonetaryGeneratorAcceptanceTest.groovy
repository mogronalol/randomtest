package com.random.acceptance

import com.random.Generator
import spock.lang.Specification

import java.util.stream.Collectors

class MonetaryGeneratorAcceptanceTest extends Specification {

    def 'Should generate a mean within two standard deviations of the uniform mean'() {
        given:
        def generator = Generator.ofMonetaryAmounts()

        when:
        def values = generator.stream().limit(100_000).collect(Collectors.toSet());

        then:
        values.each {
            assert it <= Generator.DEFAULT_MAX_MONETARY_AMOUNT
            assert it >= 0
            assert it.scale <= 2
        }
    }
}
