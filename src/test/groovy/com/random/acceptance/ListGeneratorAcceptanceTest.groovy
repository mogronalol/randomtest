package com.random.acceptance

import com.random.Generator
import spock.lang.Specification

import static java.util.stream.Collectors.toList

class ListGeneratorAcceptanceTest extends Specification {

    def 'Should generate a list only containing elements from the element generator between 5 and 15 in size'() {
        given:
        def generator = Generator.ofLists(Generator.ofOneOf(1..10))

        when:
        def generatedLists = generator.stream().limit(100_000).collect(toList())
        def sizesOfGeneratedLists = generatedLists.collect { it.size() }.toSet()

        then:
        sizesOfGeneratedLists.min() == 5
        sizesOfGeneratedLists.max() == 15
        generatedLists.flatten().toSet() == (1..10) as Set
    }

    def 'Should generate a list only containing elements from the element generator with the given size'(size) {
        given:
        def generator = Generator.ofLists(Generator.ofOneOf(1..10), {size})

        when:
        def generatedLists = generator.stream().limit(100_000).collect(toList())
        def sizesOfGeneratedLists = generatedLists.collect { it.size() }.toSet()

        then:
        sizesOfGeneratedLists.min() == size
        sizesOfGeneratedLists.max() == size
        generatedLists.flatten().toSet() == (1..10) as Set

        where:
        size << [2, 4, 7]
    }

}
