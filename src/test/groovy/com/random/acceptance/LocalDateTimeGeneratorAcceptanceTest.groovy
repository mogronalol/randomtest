package com.random.acceptance
import com.random.Generator
import com.random.utils.DiscreteUniformDistribution
import com.random.utils.MeanTester
import spock.lang.Specification

import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.temporal.ChronoUnit

import static com.google.common.collect.Range.closed
import static java.time.LocalDateTime.now
import static java.util.stream.Collectors.toSet

class LocalDateTimeGeneratorAcceptanceTest extends Specification {

    def 'Should generate random local date times'() {
        given:
        def generator = Generator.ofLocalDateTimes()

        when:
        def localDateTimes = generator.stream().limit(1000).collect(toSet());

        then:
        localDateTimes.size() == 1000
    }

    def 'Should eventually generate every date in range'() {
        given:
        def start = LocalDateTime.of(2011, 8, 4, 1, 8)
        def end = LocalDateTime.of(2011, 8, 4, 1, 12)
        def generator = Generator.ofLocalDateTimes(closed(start, end))

        when:
        def localDateTimes = generator.stream().limit(100_000).collect(toSet());

        then:
        localDateTimes.size() == ChronoUnit.SECONDS.between(start, end) + 1
        localDateTimes.min() == start
        localDateTimes.max() == end
    }

    def 'Should generate a mean within two standard deviations of the uniform mean'() {
        given:
        def start = LocalDateTime.of(2011, 8, 4, 1, 8)
        def end = LocalDateTime.of(2011, 8, 4, 1, 12)
        def generator = Generator.ofLocalDateTimes(closed(start, end));
        def distribution = DiscreteUniformDistribution.betweenLongs(start.toEpochSecond(ZoneOffset.UTC), end.toEpochSecond(ZoneOffset.UTC))
        def meanTester = new MeanTester(distribution, generator, {it.toEpochSecond(ZoneOffset.UTC).toDouble()});

        when:
        def percentage = meanTester.percentageOfTimesMeanIsWithinTwoStandardDeviationsOfUniformMean();

        then:
        percentage >= 0.95 && percentage <= 100
    }

    def 'Should generate a local date time in the future'() {
        given:
        def generator = Generator.ofFutureLocalDateTimes();

        when:
        def localDateTimes = generator.stream().limit(100_000).collect(toSet());

        then:
        localDateTimes.forEach{
            assert it > now()
        }
    }

    def 'Should generate a local date time in the past'() {
        given:
        def generator = Generator.ofPastLocalDateTimes();

        when:
        def localDateTimes = generator.stream().limit(100_000).collect(toSet());

        then:
        localDateTimes.forEach{
            assert it < now()
        }
    }
}
