package com.random.acceptance

import com.random.Generator
import com.random.utils.DiscreteUniformDistribution
import com.random.utils.MeanTester
import spock.lang.Specification

import java.util.stream.Collectors

import static com.google.common.collect.Range.*
import static java.util.stream.Collectors.toSet

class IntegerGeneratorAcceptanceTest extends Specification {

    def 'Should generate random integers'() {
        given:
        def generator = Generator.ofIntegers()

        when:
        def integers = generator.stream().limit(1000).collect(toSet());

        then:
        integers.size() == 1000
    }

    def 'Should generate a mean within two standard deviations of the uniform mean'() {
        given:
        def start = 1;
        def end = 100;
        def generator = Generator.ofIntegers(closed(start, end));
        def distribution = DiscreteUniformDistribution.betweenIntegers(start, end)
        def meanTester = new MeanTester(distribution, generator);

        when:
        def percentage = meanTester.percentageOfTimesMeanIsWithinTwoStandardDeviationsOfUniformMean();

        then:
        percentage >= 0.95 && percentage <= 100
    }

    def 'Should generate every number within range'() {
        given:
        def generator = Generator.ofIntegers(closed(100, 500));

        when:
        def results = generator.stream().limit(10_000_000).collect(Collectors.toSet());

        then:
        results.min() == 100
        results.max() == 500
        results.size() == 401
    }

    def 'Should generate within their specified ranges'(com.google.common.collect.Range<Integer> range,
                                                        def expectedMin, def expectedMax) {
        given:
        def generator = Generator.ofIntegers(range);

        when:
        def results = generator.stream().limit(10_000).collect(Collectors.toList());

        then:
        results.min() == expectedMin
        results.max() == expectedMax

        where:
        range <<       [closed(10, 20), closedOpen(10, 20), openClosed(10, 20), open(10, 20)]
        expectedMin << [10            , 10                , 11                , 11]
        expectedMax << [20            , 19                , 20                , 19]
    }
}
