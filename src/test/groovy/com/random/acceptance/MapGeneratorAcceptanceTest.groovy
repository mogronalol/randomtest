package com.random.acceptance

import com.random.Generator
import spock.lang.Specification

import static java.util.stream.Collectors.toList

class MapGeneratorAcceptanceTest extends Specification {

    def 'Should generate a map only containing elements from the element & key generators between 5 and 15 in size'() {
        given:
        def generator = Generator.ofMaps(Generator.ofOneOf(1..16), Generator.ofOneOf(20..30))

        when:
        def generatedMaps = generator.stream().limit(100_000).collect(toList())
        def sizesOfGeneratedMaps = generatedMaps.collect { it.size() }.toSet()

        then:
        sizesOfGeneratedMaps.min() == 5
        sizesOfGeneratedMaps.max() == 15
        generatedMaps.collect { it.keySet() }.flatten().toSet() == (1..16) as Set
        generatedMaps.collect { it.values() }.flatten().toSet() == (20..30) as Set
    }

    def 'Should generate a map only containing elements from the element & key generators with the given size'(size) {
        given:
        def generator = Generator.ofMaps(Generator.ofOneOf(4..19), Generator.ofOneOf(2..7), {size})

        when:
        def generatedMaps = generator.stream().limit(100_000).collect(toList())
        def sizesOfGeneratedMaps = generatedMaps.collect { it.size() }.toSet()

        then:
        sizesOfGeneratedMaps.min() == size
        sizesOfGeneratedMaps.max() == size
        generatedMaps.collect { it.keySet() }.flatten().toSet() == (4..19) as Set
        generatedMaps.collect { it.values() }.flatten().toSet() == (2..7) as Set

        where:
        size << [2, 4, 7]
    }
}
