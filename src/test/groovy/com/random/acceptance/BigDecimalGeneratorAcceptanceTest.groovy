package com.random.acceptance

import com.random.Generator
import com.random.utils.DiscreteUniformDistribution
import com.random.utils.MeanTester
import spock.lang.Specification

import java.util.stream.Collectors

import static com.google.common.collect.Range.*
import static java.util.stream.Collectors.toSet

class BigDecimalGeneratorAcceptanceTest extends Specification {

    def 'Should generate random big decimals'() {
        given:
        def generator = Generator.ofBigDecimals()

        when:
        def bigDecimals = generator.stream().limit(1000).collect(toSet());

        then:
        bigDecimals.size() == 1000
    }

    def 'Should generate a mean within two standard deviations of the uniform mean'() {
        given:
        def start = 0.18
        def end = 0.21
        def scale = Generator.DEFAULT_DECIMAL_SCALE
        def generator = Generator.ofBigDecimals(closed(start, end))
        def distribution = DiscreteUniformDistribution.betweenDecimalsAsIntegers(start, end, scale)
        def meanTester = new MeanTester(distribution, generator, {it.movePointRight(scale).toDouble()})

        when:
        def percentage = meanTester.percentageOfTimesMeanIsWithinTwoStandardDeviationsOfUniformMean()

        then:
        percentage >= 0.95 && percentage <= 100
    }

    def 'Should generate every number within range using correct scale'(final def generator,
                                                                        final def expectedMin,
                                                                        final def expectedMax,
                                                                        final def expectedSize,
                                                                        final def expectedScale) {
        when:
        final def results = generator.stream().limit(10_000_000).collect(toSet())

        then:
        results.min() == expectedMin
        results.max() == expectedMax
        results.size() == expectedSize
        results.each {
            assert it.toBigDecimal().scale <= expectedScale
        }

        where:
        generator    << [Generator.ofBigDecimals(closed(0.25, 0.56)), Generator.ofBigDecimals(closedOpen(0.0058, 0.012), 5)]
        expectedMin  << [0.25                             , 0.0058]
        expectedMax  << [0.56                             , 0.01199]
        expectedSize << [3101                             , 620]
        expectedScale <<[Generator.DEFAULT_DECIMAL_SCALE  , 5]
    }

    def 'Should generate BigDecimals within their specified ranges'(final com.google.common.collect.Range<Double> range,
                                                        final BigDecimal expectedMin, final BigDecimal expectedMax) {
        given:
        final Generator<Double> generator = Generator.ofBigDecimals(range)

        when:
        final def results = generator.stream().limit(10_000).collect(Collectors.toList())

        then:
        results.min() == expectedMin
        results.max() == expectedMax

        where:
        range       << [closed(0.01, 0.05) , closedOpen(0.02, 0.06) , openClosed(0.03, 0.04) , open(0.041, 0.0481)]
        expectedMin << [0.01               , 0.02                   , 0.0301                 , 0.0411]
        expectedMax << [0.05               , 0.0599                 , 0.04                   , 0.048]
    }

    def 'Should generate BigDecimals with the specified scale'(final Generator<BigDecimal> generator,
                                                               final int expectedScale) {
        when:
        final def results = generator.stream().limit(1000).collect(toSet())

        then:
        results.each {
            assert it.toBigDecimal().scale <= expectedScale
        }

        where:
        generator      << [Generator.ofBigDecimals(1), Generator.ofBigDecimals(2), Generator.ofBigDecimals(5)]
        expectedScale  << [1           , 2           , 5]
    }
}
