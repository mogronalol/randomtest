package com.random.acceptance
import com.random.Generator
import com.random.utils.DiscreteUniformDistribution
import com.random.utils.MeanTester
import spock.lang.Specification

import java.util.stream.Collectors

class EnumGeneratorAcceptanceTest extends Specification {
    def 'Should generate a mean within two standard deviations of the uniform mean'() {
        given:
        def generator = Generator.ofEnums(StubEnum)
        def distribution = DiscreteUniformDistribution.betweenIntegers(1, 10, 10)
        def meanTester = new MeanTester(distribution, generator, {Double.valueOf(it.value)})

        when:
        def percentage = meanTester.percentageOfTimesMeanIsWithinTwoStandardDeviationsOfUniformMean()

        then:
        percentage >= 0.95 && percentage <= 100
    }

    def 'Should generate every element'() {
        given:
        def generator = Generator.ofEnums(StubEnum)

        when:
        def elements = generator.stream().limit(100_000).collect(Collectors.toSet())

        then:
        elements.size() == 10
    }

    private enum StubEnum {
        ONE(1), TWO(2), THREE(3), FOUR(4), FIVE(5), SIX(6), SEVEN(7), EIGHT(8), NINE(9), TEN(10)

        double value

        StubEnum(double value) {
            this.value = value
        }
    }
}
