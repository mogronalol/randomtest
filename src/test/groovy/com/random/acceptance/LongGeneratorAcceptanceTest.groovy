package com.random.acceptance

import com.random.Generator
import com.random.utils.DiscreteUniformDistribution
import com.random.utils.MeanTester
import spock.lang.Specification

import java.util.stream.Collectors

import static com.google.common.collect.Range.*
import static java.util.stream.Collectors.toSet

class LongGeneratorAcceptanceTest extends Specification {

    def 'Should generate random longs'() {
        given:
        def generator = Generator.ofLongs()

        when:
        def longs = generator.stream().limit(1000).collect(toSet());

        then:
        longs.size() == 1000
    }

    def 'Should generate a mean within two standard deviations of the uniform mean'() {
        given:
        def start = 1L
        def end = 100L
        def generator = Generator.ofLongs(closed(start, end))
        def distribution = DiscreteUniformDistribution.betweenLongs(start, end)
        def meanTester = new MeanTester(distribution, generator)

        when:
        def percentage = meanTester.percentageOfTimesMeanIsWithinTwoStandardDeviationsOfUniformMean();

        then:
        percentage >= 0.95 && percentage <= 100
    }

    def 'Should generate every number within range'() {
        given:
        def generator = Generator.ofLongs(closed(100L, 500L));

        when:
        def results = generator.stream().limit(10_000_000).collect(Collectors.toSet());

        then:
        results.min() == 100
        results.max() == 500
        results.size() == 401
    }

    def 'Should generate within their specified ranges'(com.google.common.collect.Range<Long> range,
                                                        int expectedMin, int expectedMax) {
        given:
        def generator = Generator.ofLongs(range);

        when:
        def results = generator.stream().limit(10_000).collect(Collectors.toList());

        then:
        results.min() == expectedMin
        results.max() == expectedMax

        where:
        range <<       [closed(10L, 20L), closedOpen(10L, 20L), openClosed(10L, 20L), open(10L, 20L)]
        expectedMin << [10L            , 10L                , 11L                , 11L]
        expectedMax << [20L            , 19L                , 20L                , 19L]
    }
}
