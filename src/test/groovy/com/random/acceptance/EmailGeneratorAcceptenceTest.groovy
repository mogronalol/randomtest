package com.random.acceptance
import com.random.Generator
import spock.lang.Specification

import static java.util.stream.Collectors.toSet

class EmailGeneratorAcceptenceTest extends Specification {
    def 'Should generate a different email address every time'() {
        given:
        def generator = Generator.ofEmailAddresses()

        when:
        def emailAddresses = generator.stream().limit(1000).collect(toSet())

        then:
        emailAddresses.size() == 1000
    }

    def 'Should only generate strings which match email address pattern'() {
        given:
        def generator = Generator.ofEmailAddresses()

        when:
        def emailAddresses = generator.stream().limit(1000).collect(toSet())

        then:
        emailAddresses.forEach {
            println(it)
            assert it ==~ /^[a-zA-Z0-9]{5,20}@[a-zA-Z0-9]{5,10}.(com|co.uk|uk|biz|gov.uk|ca)$/
        }
    }
}
