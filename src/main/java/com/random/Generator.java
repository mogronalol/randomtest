package com.random;

import com.google.common.collect.Range;
import com.random.collections.ListGenerator;
import com.random.collections.MapGenerator;
import com.random.collections.SetGenerator;
import com.random.datetime.LocalDateGenerator;
import com.random.datetime.LocalDateTimeGenerator;
import com.random.datetime.LocalTimeGenerator;
import com.random.misc.*;
import com.random.number.BigDecimalGenerator;
import com.random.number.DoubleGenerator;
import com.random.number.IntegerGenerator;
import com.random.number.LongGenerator;
import org.apache.commons.math3.random.RandomDataGenerator;

import java.math.BigDecimal;
import java.time.*;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Stream;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Range.*;
import static java.time.LocalDate.now;

public interface Generator<T> {

    public T next();

    default Stream<T> stream() {
        return Stream.generate(() -> Generator.this.next());
    }

    static final RandomDataGenerator randomDataGenerator = new RandomDataGenerator();
    EmailAddressGenerator EMAIL_ADDRESS_GENERATOR = new EmailAddressGenerator(new StringGenerator(ofIntegers(closed(5, 20))),
            new StringGenerator(ofIntegers(closed(5, 10))),
            new OneOfGenerator(randomDataGenerator, "com", "uk", "co.uk", "biz", "gov.uk", "ca"));
    LocalTimeGenerator DEFAULT_PAST_LOCAL_TIME_GENERATOR = new LocalTimeGenerator(randomDataGenerator, lessThan(LocalTime.now().withNano(0)));
    LocalTimeGenerator DEFAULT_FUTURE_LOCAL_TIME_GENERATOR = new LocalTimeGenerator(randomDataGenerator, greaterThan(LocalTime.now().withNano(0)));
    LocalTimeGenerator DEFAULT_LOCAL_TIME_GENERATOR = new LocalTimeGenerator(randomDataGenerator, all());
    LocalDateTimeGenerator DEFAULT_PAST_LOCAL_DATE_TIME_GENERATOR = new LocalDateTimeGenerator(randomDataGenerator, lessThan(LocalDateTime.now().withNano(0)));
    LocalDateTimeGenerator DEFAULT_FUTURE_LOCAL_DATE_TIME_GENERATOR = new LocalDateTimeGenerator(randomDataGenerator, greaterThan(LocalDateTime.now().withNano(0)));
    LocalDateTimeGenerator DEFAULT_LOCAL_DATE_TIME_GENERATOR = new LocalDateTimeGenerator(randomDataGenerator, all());
    LocalDateGenerator DEFAULT_PAST_LOCAL_DATE_GENERATOR = new LocalDateGenerator(randomDataGenerator, lessThan(now()));
    LocalDateGenerator DEFAULT_FUTURE_LOCAL_DATE_GENERATOR = new LocalDateGenerator(randomDataGenerator, greaterThan(now()));
    LocalDateGenerator DEFAULT_LOCAL_DATE_GENERATOR = new LocalDateGenerator(randomDataGenerator, all());
    public static final Random random = new Random();
    BooleanGenerator DEFAULT_BOOLEAN_GENERATOR = new BooleanGenerator(random);

    static final BigDecimal DEFAULT_MAX_MONETARY_AMOUNT = new BigDecimal(9999999);
    BigDecimalGenerator DEFAULT_MONETARY_AMOUNT_GENERATOR = new BigDecimalGenerator(randomDataGenerator, Range.closed(BigDecimal.ZERO, DEFAULT_MAX_MONETARY_AMOUNT), 2);
    static final int DEFAULT_DECIMAL_SCALE = 4;
    BigDecimalGenerator DEFAULT_BIG_DECIMAL_GENERATOR = new BigDecimalGenerator(randomDataGenerator, all(), DEFAULT_DECIMAL_SCALE);

    static final DoubleGenerator DEFAULT_DOUBLE_GENERATOR = new DoubleGenerator(randomDataGenerator, Range.<Double>all(), DEFAULT_DECIMAL_SCALE);
    static final LongGenerator DEFAULT_LONG_GENERATOR = new LongGenerator(randomDataGenerator, Range.<Long>all());
    static final IntegerGenerator DEFAULT_INTEGER_GENERATOR = new IntegerGenerator(randomDataGenerator, Range.<Integer>all());
    static final IntegerGenerator DEFAULT_STRING_SIZE_GENERATOR = new IntegerGenerator(randomDataGenerator, Range.closed(5, 15));
    StringGenerator DEFAULT_STRING_GENERATOR = new StringGenerator(DEFAULT_STRING_SIZE_GENERATOR);
    static final IntegerGenerator DEFAULT_COLLECTION_SIZE_GENERATOR = new IntegerGenerator(randomDataGenerator, Range.closed(5, 15));



    // Integer

    public static Generator<Integer> ofIntegers() {
        return DEFAULT_INTEGER_GENERATOR;
    }

    public static Generator<Integer> ofIntegers(final Range<Integer> range) {
        return new IntegerGenerator(randomDataGenerator, range);
    }

    // Longs

    public static Generator<Long> ofLongs() {
        return DEFAULT_LONG_GENERATOR;
    }

    public static Generator<Long> ofLongs(final Range<Long> range) {
        return new LongGenerator(randomDataGenerator, range);
    }

    //Doubles

    public static Generator<Double> ofDoubles() {
        return DEFAULT_DOUBLE_GENERATOR;
    }

    public static Generator<Double> ofDoubles(final Range<Double> range) {
        return new DoubleGenerator(randomDataGenerator, range, DEFAULT_DECIMAL_SCALE);
    }

    public static Generator<Double> ofDoubles(final int scale) {
        return new DoubleGenerator(randomDataGenerator, Range.<Double>all(), scale);
    }

    public static Generator<Double> ofDoubles(final Range<Double> range, final int scale) {
        return new DoubleGenerator(randomDataGenerator, range, scale);
    }

    // Monetary

    public static Generator<BigDecimal> ofMonetaryAmounts() {
        return DEFAULT_MONETARY_AMOUNT_GENERATOR;
    }

    // Big decimals
    public static Generator<BigDecimal> ofBigDecimals() {
        return DEFAULT_BIG_DECIMAL_GENERATOR;
    }

    public static Generator<BigDecimal> ofBigDecimals(final int scale) {
        return new BigDecimalGenerator(randomDataGenerator, Range.<BigDecimal>all(), scale);
    }

    public static Generator<BigDecimal> ofBigDecimals(final Range<BigDecimal> range) {
        return new BigDecimalGenerator(randomDataGenerator, range, DEFAULT_DECIMAL_SCALE);
    }

    public static Generator<BigDecimal> ofBigDecimals(final Range<BigDecimal> range, final int scale) {
        return new BigDecimalGenerator(randomDataGenerator, range, scale);
    }

    // Boolean

    public static Generator<Boolean> ofBooleans() {
        return DEFAULT_BOOLEAN_GENERATOR;
    }

    public static <T> Generator<T> ofOneOf(final Iterable<? extends T> iterables) {
        return new OneOfGenerator<>(randomDataGenerator, iterables);
    }

    public static <T> Generator<T> ofOneOf(final T ... elements) {
        return ofOneOf(newArrayList(elements));
    }

    // Enum

    public static <T extends Enum<T>> Generator<T> ofEnums(final Class<T> enumStubClass) {
        return new OneOfGenerator<>(randomDataGenerator, newArrayList(enumStubClass.getEnumConstants()));
    }

    // Of singleton

    public static <T> Generator<T> ofSingleton(final T singleton) {
        return new SingletonGenerator<>(singleton);
    }

    // Strings

    public static Generator<String> ofStrings() {
        return DEFAULT_STRING_GENERATOR;
    }

    public static Generator<String> ofStrings(final Generator<Integer> sizeGenerator) {
        return new StringGenerator(sizeGenerator);
    }

    // Lists

    public static <T> Generator<List<T>> ofLists(final Generator<? extends T> elementGenerator) {
        return new ListGenerator<>(elementGenerator, DEFAULT_COLLECTION_SIZE_GENERATOR);
    }

    public static <T> Generator<List<T>> ofLists(final Generator<? extends T> elementGenerator, final Generator<Integer> sizeGenerator) {
        return new ListGenerator<>(elementGenerator, sizeGenerator);
    }

    // Sets

    public static <T> Generator<Set<T>> ofSets(final Generator<? extends T> elementGenerator) {
        return new SetGenerator<>(elementGenerator, DEFAULT_COLLECTION_SIZE_GENERATOR);
    }

    public static <T> Generator<Set<T>> ofSets(final Generator<? extends T> elementGenerator, final Generator<Integer> sizeGenerator) {
        return new SetGenerator<>(elementGenerator, sizeGenerator);
    }

    // Maps

    public static <K, V> Generator<Map<K, V>> ofMaps(final Generator<? extends K> keyGenerator, final Generator<? extends V> valueGenerator) {
        return new MapGenerator<>(keyGenerator, valueGenerator, DEFAULT_COLLECTION_SIZE_GENERATOR);
    }

    public static <K, V> Generator<Map<K, V>> ofMaps(final Generator<? extends K> keyGenerator, final Generator<? extends V> valueGenerator, Generator<Integer> sizeGenerator) {
        return new MapGenerator<>(keyGenerator, valueGenerator, sizeGenerator);
    }

    // Local Dates

    public static Generator<LocalDate> ofLocalDates() {
        return DEFAULT_LOCAL_DATE_GENERATOR;
    }

    public static Generator<LocalDate> ofLocalDates(Range<LocalDate> range) {
        return new LocalDateGenerator(randomDataGenerator, range);
    }

    public static Generator<LocalDate> ofFutureLocalDates() {
        return DEFAULT_FUTURE_LOCAL_DATE_GENERATOR;
    }

    public static Generator<LocalDate> ofFutureLocalDatesUpTo(final Period period) {
        return new LocalDateGenerator(randomDataGenerator, openClosed(now(), now().plus(period)));
    }

    public static Generator<LocalDate> ofPastLocalDates() {
        return DEFAULT_PAST_LOCAL_DATE_GENERATOR;
    }

    public static Generator<LocalDate> ofPastLocalDatesUpTo(final Period period) {
        return new LocalDateGenerator(randomDataGenerator, closedOpen(now().minus(period), now()));
    }

    // Local Date times

    public static Generator<LocalDateTime> ofLocalDateTimes() {
        return DEFAULT_LOCAL_DATE_TIME_GENERATOR;
    }

    public static Generator<LocalDateTime> ofLocalDateTimes(Range<LocalDateTime> range) {
        return new LocalDateTimeGenerator(randomDataGenerator, range);
    }

    public static Generator<LocalDateTime> ofFutureLocalDateTimes() {
        return DEFAULT_FUTURE_LOCAL_DATE_TIME_GENERATOR;
    }

    public static Generator<LocalDateTime> ofFutureLocalDateTimesUpTo(final Duration duration) {
        return new LocalDateTimeGenerator(randomDataGenerator, openClosed(LocalDateTime.now().withNano(0), LocalDateTime.now().plus(duration)));
    }

    public static Generator<LocalDateTime> ofPastLocalDateTimes() {
        return DEFAULT_PAST_LOCAL_DATE_TIME_GENERATOR;
    }

    public static Generator<LocalDateTime> ofPastLocalDateTimesUpTo(final Duration duration) {
        return new LocalDateTimeGenerator(randomDataGenerator, closedOpen(LocalDateTime.now().withNano(0).minus(duration), LocalDateTime.now().withNano(0)));
    }

    // Local times

    public static Generator<LocalTime> ofLocalTimes() {
        return DEFAULT_LOCAL_TIME_GENERATOR;
    }

    public static Generator<LocalTime> ofLocalTimes(Range<LocalTime> range) {
        return new LocalTimeGenerator(randomDataGenerator, range);
    }

    public static Generator<LocalTime> ofFutureLocalTimes() {
        return DEFAULT_FUTURE_LOCAL_TIME_GENERATOR;
    }

    public static Generator<LocalTime> ofFutureLocalTimesUpTo(final Duration duration) {
        return new LocalTimeGenerator(randomDataGenerator, openClosed(LocalTime.now().withNano(0), LocalTime.now().plus(duration)));
    }

    public static Generator<LocalTime> ofPastLocalTimes() {
        return DEFAULT_PAST_LOCAL_TIME_GENERATOR;
    }

    public static Generator<LocalTime> ofPastLocalTimesUpTo(final Duration duration) {
        return new LocalTimeGenerator(randomDataGenerator, closedOpen(LocalTime.now().withNano(0).minus(duration), LocalTime.now().withNano(0)));
    }

    //Email

    public static Generator<String> ofEmailAddresses() {
        return EMAIL_ADDRESS_GENERATOR;
    }
}
