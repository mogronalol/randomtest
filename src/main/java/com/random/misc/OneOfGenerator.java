package com.random.misc;

import com.google.common.collect.Iterables;
import com.google.common.collect.Range;
import com.random.Generator;
import com.random.number.IntegerGenerator;
import org.apache.commons.math3.random.RandomDataGenerator;

import static com.google.common.collect.Lists.newArrayList;

public class OneOfGenerator<T> implements Generator<T> {
    private final Iterable<? extends T> objects;
    private final RandomDataGenerator randomDataGenerator;

    public OneOfGenerator(final RandomDataGenerator randomDataGenerator, final Iterable<? extends T> objects) {
        this.objects = objects;
        this.randomDataGenerator = randomDataGenerator;
    }

    public OneOfGenerator(final RandomDataGenerator randomDataGenerator, final T ... objects) {
        this(randomDataGenerator, newArrayList(objects));
    }

    @Override
    public T next() {
        final int position = new IntegerGenerator(randomDataGenerator, Range.closedOpen(0, Iterables.size(objects))).next();
        return Iterables.get(objects, position);
    }
}
