package com.random.misc;

import com.random.Generator;

public class SingletonGenerator<T> implements Generator<T> {
    private final T singleton;

    public SingletonGenerator(final T singleton) {
        this.singleton = singleton;
    }

    @Override
    public T next() {
        return singleton;
    }
}
