package com.random.misc;

import com.random.Generator;

import java.util.Random;

public class BooleanGenerator implements Generator<Boolean> {

    private final Random random;

    public BooleanGenerator(final Random random) {
        this.random = random;
    }

    @Override
    public Boolean next() {
        return random.nextBoolean();
    }
}
