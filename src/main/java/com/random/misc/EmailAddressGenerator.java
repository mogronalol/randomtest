package com.random.misc;

import com.random.Generator;

public class EmailAddressGenerator implements Generator<String> {

    private final Generator<String> leftSideGenerator;
    private final Generator<String> rightSideGenerator;
    private final Generator<String> suffixGenerator;

    public EmailAddressGenerator(final Generator<String> leftSideGenerator,
                                 final Generator<String> rightSideGenerator,
                                 final Generator<String> suffixGenerator) {
        this.leftSideGenerator = leftSideGenerator;
        this.rightSideGenerator = rightSideGenerator;
        this.suffixGenerator = suffixGenerator;
    }

    @Override
    public String next() {
        return leftSideGenerator.next() + "@" + rightSideGenerator.next() + "." + suffixGenerator.next();
    }
}
