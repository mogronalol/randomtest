package com.random.misc;

import com.random.Generator;
import org.apache.commons.lang3.RandomStringUtils;

public class StringGenerator implements Generator<String> {

    private final Generator<Integer> sizeGenerator;

    public StringGenerator(final Generator<Integer> sizeGenerator) {
        this.sizeGenerator = sizeGenerator;
    }

    @Override
    public String next() {
        return RandomStringUtils.random(sizeGenerator.next(), true, true);
    }
}
