package com.random.number;

import com.google.common.collect.Range;
import com.random.AbstractRangeGenerator;
import org.apache.commons.math3.random.RandomDataGenerator;

public class IntegerGenerator extends AbstractRangeGenerator<Integer> {

    private final RandomDataGenerator randomDataGenerator;

    public IntegerGenerator(final RandomDataGenerator randomDataGenerator, final Range<Integer> range) {
        super(range, Integer.MIN_VALUE, Integer.MAX_VALUE);
        this.randomDataGenerator = randomDataGenerator;
    }

    @Override
    protected Integer increment(final Integer integer) {
        return integer + 1;
    }

    @Override
    protected Integer decrement(final Integer integer) {
        return integer - 1;
    }

    @Override
    protected Integer generate(final Integer lowerBound, final Integer upperBound) {
        return randomDataGenerator.nextInt(lowerBound, upperBound);
    }
}
