package com.random.number;

import com.google.common.collect.Range;
import com.random.AbstractRangeGenerator;
import org.apache.commons.math3.random.RandomDataGenerator;

import java.math.BigDecimal;

public class BigDecimalGenerator extends AbstractRangeGenerator<BigDecimal> {

    private final RandomDataGenerator randomDataGenerator;
    private final int scale;
    private final BigDecimal oneUnit;

    public BigDecimalGenerator(final RandomDataGenerator randomDataGenerator, final Range<BigDecimal> range, final int scale) {
        super(range, BigDecimal.valueOf(-Double.MAX_VALUE).setScale(scale, BigDecimal.ROUND_HALF_UP),
                BigDecimal.valueOf(Double.MAX_VALUE).setScale(scale, BigDecimal.ROUND_HALF_UP));

        this.randomDataGenerator = randomDataGenerator;
        this.scale = scale;
        this.oneUnit = BigDecimal.ONE.scaleByPowerOfTen(-scale);
    }

    @Override
    protected BigDecimal increment(final BigDecimal value) {
        return value.add(oneUnit);
    }

    @Override
    protected BigDecimal decrement(final BigDecimal value) {
        return value.subtract(oneUnit);
    }

    @Override
    protected BigDecimal generate(final BigDecimal lowerBound, final BigDecimal upperBound) {
        return BigDecimal.valueOf(randomDataGenerator.nextUniform(
                lowerBound.setScale(scale, BigDecimal.ROUND_HALF_UP).doubleValue(), upperBound.setScale(scale, BigDecimal.ROUND_HALF_UP).doubleValue()))
                .setScale(scale, BigDecimal.ROUND_HALF_UP);
    }
}
