package com.random.number;

import com.google.common.collect.Range;
import com.random.AbstractRangeGenerator;
import org.apache.commons.math3.random.RandomDataGenerator;

public class LongGenerator extends AbstractRangeGenerator<Long> {

    private final RandomDataGenerator randomDataGenerator;

    public LongGenerator(final RandomDataGenerator randomDataGenerator, final Range<Long> range) {
        super(range, Long.MIN_VALUE, Long.MAX_VALUE);
        this.randomDataGenerator = randomDataGenerator;
    }

    @Override
    protected Long increment(final Long value) {
        return value + 1;
    }

    @Override
    protected Long decrement(final Long value) {
        return value - 1;
    }

    @Override
    protected Long generate(final Long lowerBound, final Long upperBound) {
        return randomDataGenerator.nextLong(lowerBound, upperBound);
    }
}
