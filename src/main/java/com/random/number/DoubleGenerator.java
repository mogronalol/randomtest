package com.random.number;

import com.google.common.collect.Range;
import com.random.AbstractRangeGenerator;
import org.apache.commons.math3.random.RandomDataGenerator;

import java.math.BigDecimal;

public class DoubleGenerator extends AbstractRangeGenerator<Double> {

    private final RandomDataGenerator randomDataGenerator;
    private final int SCALE;
    private final double oneUnit;

    public DoubleGenerator(final RandomDataGenerator randomDataGenerator, final Range<Double> range, final int scale) {
        super(range, -Double.MAX_VALUE, Double.MAX_VALUE);
        this.randomDataGenerator = randomDataGenerator;
        SCALE = scale;
        oneUnit = BigDecimal.ONE.scaleByPowerOfTen(-SCALE).doubleValue();
    }

    @Override
    protected Double increment(final Double value) {
        return value + oneUnit;
    }

    @Override
    protected Double decrement(final Double value) {
        return value - oneUnit;
    }

    @Override
    protected Double generate(final Double lowerBound, final Double upperBound) {
        return BigDecimal.valueOf(randomDataGenerator.nextUniform(lowerBound, upperBound))
                .setScale(SCALE, BigDecimal.ROUND_HALF_UP)
                .doubleValue();
    }
}
