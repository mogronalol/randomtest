package com.random.collections;

import com.random.Generator;

import java.util.Set;
import java.util.stream.Collectors;

public class SetGenerator<T> implements Generator<Set<T>>{

    private final Generator<? extends T> elementGenerator;
    private final Generator<Integer> sizeGenerator;

    public SetGenerator(final Generator<? extends T> elementGenerator, final Generator<Integer> sizeGenerator) {

        this.elementGenerator = elementGenerator;
        this.sizeGenerator = sizeGenerator;
    }

    @Override
    public Set<T> next() {
        return elementGenerator.stream().distinct().limit(sizeGenerator.next()).collect(Collectors.toSet());
    }
}
