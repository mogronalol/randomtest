package com.random.collections;

import com.random.Generator;

import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;

public class MapGenerator<K, V> implements Generator<Map<K, V>>{
    private final Generator<? extends K> keyGenerator;
    private final Generator<? extends V> valueGenerator;
    private final Generator<Integer> sizeGenerator;

    public MapGenerator(final Generator<? extends K> keyGenerator, final Generator<? extends V> valueGenerator, final Generator<Integer> sizeGenerator) {
        this.keyGenerator = keyGenerator;
        this.valueGenerator = valueGenerator;
        this.sizeGenerator = sizeGenerator;
    }

    @Override
    public Map<K, V> next() {
        final Map<K, V> map = newHashMap();
        final Integer size = sizeGenerator.next();

        while(map.size() < size) {
            map.put(keyGenerator.next(), valueGenerator.next());
        }

        return map;
    }
}
