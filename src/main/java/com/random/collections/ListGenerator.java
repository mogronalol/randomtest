package com.random.collections;

import com.random.Generator;

import java.util.List;
import java.util.stream.Collectors;

public class ListGenerator<T> implements Generator<List<T>> {

    private final Generator<? extends T> elementGenerator;
    private final Generator<Integer> sizeGenerator;

    public ListGenerator(final Generator<? extends T> elementGenerator, final Generator<Integer> sizeGenerator) {
        this.elementGenerator = elementGenerator;
        this.sizeGenerator = sizeGenerator;
    }

    @Override
    public List<T> next() {
        return elementGenerator.stream()
                .limit(sizeGenerator.next())
                .collect(Collectors.toList());
    }
}
