package com.random.datetime;

import com.google.common.collect.Range;
import com.random.AbstractRangeGenerator;
import org.apache.commons.math3.random.RandomDataGenerator;

import java.time.LocalTime;

public class LocalTimeGenerator extends AbstractRangeGenerator<LocalTime> {

    private final RandomDataGenerator randomDataGenerator;

    public LocalTimeGenerator(final RandomDataGenerator randomDataGenerator, final Range<LocalTime> range) {
        super(range, LocalTime.MIN, LocalTime.MAX);
        this.randomDataGenerator = randomDataGenerator;
    }

    @Override
    protected LocalTime increment(final LocalTime LocalTime) {
        return LocalTime.plusSeconds(1);
    }

    @Override
    protected LocalTime decrement(final LocalTime LocalTime) {
        return LocalTime.minusSeconds(1);
    }

    @Override
    protected LocalTime generate(final LocalTime lowerBound, final LocalTime upperBound) {
        return LocalTime.ofSecondOfDay(
                randomDataGenerator.nextLong(lowerBound.toSecondOfDay(), upperBound.toSecondOfDay()));
    }
}
