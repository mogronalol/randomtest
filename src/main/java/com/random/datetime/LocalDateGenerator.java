package com.random.datetime;

import com.google.common.collect.Range;
import com.random.AbstractRangeGenerator;
import org.apache.commons.math3.random.RandomDataGenerator;

import java.time.LocalDate;

import static java.time.LocalDate.MAX;
import static java.time.LocalDate.MIN;

public class LocalDateGenerator extends AbstractRangeGenerator<LocalDate> {

    private final RandomDataGenerator randomDataGenerator;

    public LocalDateGenerator(final RandomDataGenerator randomDataGenerator, final Range<LocalDate> range) {
        super(range, MIN, MAX);
        this.randomDataGenerator = randomDataGenerator;
    }

    @Override
    protected LocalDate increment(final LocalDate date) {
        return date.plusDays(1);
    }

    @Override
    protected LocalDate decrement(final LocalDate date) {
        return date.minusDays(1);
    }

    @Override
    protected LocalDate generate(final LocalDate lowerBound, final LocalDate upperBound) {
        final long day = randomDataGenerator.nextLong(lowerBound.toEpochDay(), upperBound.toEpochDay());
        return LocalDate.ofEpochDay(day);
    }
}
