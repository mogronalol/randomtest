package com.random.datetime;

import com.google.common.collect.Range;
import com.random.AbstractRangeGenerator;
import org.apache.commons.math3.random.RandomDataGenerator;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class LocalDateTimeGenerator extends AbstractRangeGenerator<LocalDateTime> {

    private final RandomDataGenerator randomDataGenerator;

    public LocalDateTimeGenerator(final RandomDataGenerator randomDataGenerator, final Range<LocalDateTime> range) {
        super(range, LocalDateTime.MIN, LocalDateTime.MAX);
        this.randomDataGenerator = randomDataGenerator;
    }

    @Override
    protected LocalDateTime increment(final LocalDateTime localDateTime) {
        return localDateTime.plusSeconds(1);
    }

    @Override
    protected LocalDateTime decrement(final LocalDateTime localDateTime) {
        return localDateTime.minusSeconds(1);
    }

    @Override
    protected LocalDateTime generate(final LocalDateTime lowerBound, final LocalDateTime upperBound) {
        return LocalDateTime.ofEpochSecond(randomDataGenerator.nextLong(
                lowerBound.toEpochSecond(ZoneOffset.UTC), upperBound.toEpochSecond(ZoneOffset.UTC)),
                0,
                ZoneOffset.UTC).withNano(0);
    }
}
