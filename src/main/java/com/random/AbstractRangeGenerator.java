package com.random;

import com.google.common.collect.Range;

import static com.google.common.collect.BoundType.CLOSED;

public abstract class AbstractRangeGenerator<T extends Comparable<? super T>> implements Generator<T> {
    private final Range<T> range;
    private final T defaultMinValue;
    private final T defaultMaxValue;

    public AbstractRangeGenerator(final Range<T> range, T defaultMinValue, T defaultMaxValue) {
        this.range = range;
        this.defaultMinValue = defaultMinValue;
        this.defaultMaxValue = defaultMaxValue;
    }

    @Override
    public T next() {
        if(lowerBound().compareTo(upperBound()) == 0) return lowerBound();
        return generate(lowerBound(), upperBound());
    }

    private T lowerBound() {
        if (!range.hasLowerBound()) {
            return defaultMinValue;
        }
        return range.lowerBoundType() == CLOSED ? range.lowerEndpoint() : increment(range.lowerEndpoint());
    }

    private T upperBound() {
        if (!range.hasUpperBound()) {
            return defaultMaxValue;
        }
        return range.upperBoundType() == CLOSED ? range.upperEndpoint() : decrement(range.upperEndpoint());
    }

    protected abstract T increment(final T t);

    protected abstract T decrement(final T t);

    protected abstract T generate(T lowerBound, T upperBound);
}
