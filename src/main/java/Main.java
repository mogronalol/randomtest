import com.google.common.collect.Range;
import com.random.Generator;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.google.common.collect.Range.closed;
import static com.random.Generator.*;
import static com.random.Generator.ofMaps;
import static com.random.Generator.ofOneOf;
import static java.time.Duration.ofDays;
import static java.util.stream.Collectors.toSet;


public class Main {
    public static void main(String[] varArgs) {

        //Some examples of how to generate data

        final String generatedString = ofStrings().next();

        final Integer generatedInteger = ofIntegers(closed(1, 20)).next();

        final Long generatedLong = ofLongs().next();

        final LocalDate generatedFutureDate = ofFutureLocalDates().next();

        final LocalDateTime generatedPastDate = ofPastLocalDateTimesUpTo(ofDays(1)).next();

        final Map<String, Integer> generatedMap = ofMaps(ofStrings(), ofIntegers()).next();

        final String generatedElement = ofOneOf(generatedMap.keySet()).next();

        final List<Object> objectList = ofLists(() -> new Object()).next();

        final Set<LocalDate> generatedPastLocalDates = ofPastLocalDates().stream().limit(1000).collect(toSet());

    }
}
